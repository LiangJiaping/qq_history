<?php
/**
 * Created by PhpStorm.
 * User: https://pingxonline.com/
 * Date: 2018-08-12
 * Time: 9:40
 */

/**
 * Class config
 *
 * 基本配置
 *
 */
class config{
    // QQ和密码，获取cookies用，可以用自己的小号来登录，只是为了获取cookie，账号是谁的不重要
    public static $qq = "";
    public static $password = "";

    // 登录获取cookies的地址，无需修改
    public static $url = "https://xui.ptlogin2.qq.com/cgi-bin/xlogin?appid=715030902&daid=70&pt_no_auth=0&s_url=http://qzone.qq.com";

    // Selenium 服务器地址，默认值无需修改
    public static $server = "http://localhost:4444/wd/hub";

    // 保存和读取uin和skey的文件名，无需修改，请注意当前文件夹的读取和写入权限
    public static $filename = "cookies.php";

    // 请求的重试次数
    public static $retry_time = 4;
}