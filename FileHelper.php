<?php
/**
 * Created by PhpStorm.
 * User: https://pingxonline.com/
 * Date: 2018-08-12
 * Time: 16:00
 *
 * 保存获取的uin和skey
 *
 */

class FileHelper
{

    // 写入
    /**
     * @param $filename
     * @param $cookies
     */
    public static function writer($filename, $cookies){
        $fp = fopen($filename, 'w');
        fwrite($fp,"<?php exit(); \r\n");
        fwrite($fp,json_encode($cookies));
        fclose($fp);
    }

    // 读取
    /**
     * @param $filename
     * @return mixed
     */
    public static function reader($filename){
        $fp = fopen($filename, 'r');
        $arr = array();
        $num = 1;
        $cookies  = null;
        while(!feof($fp)){
            $line = trim(fgets($fp));
            if($num == 2){
                $cookies = str_replace("\r","",$line);
                $cookies = str_replace("\n","",$line);
            }
            $arr[] = $line;
            $num++;
        }

//        echo $cookies;
        fclose($fp);
        if (empty($cookies)){
            return null;
        }else{
            return json_decode($cookies, true);
        }
    }
}