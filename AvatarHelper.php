<?php
/**
 * Created by PhpStorm.
 * User: https://pingxonline.com/
 * Date: 2018-08-12
 * Time: 12:13
 */

class AvatarHelper
{
    /**
     * @param $cookie 获取uin和skey
     * @return mixed 返回结果
     */
    public function avatar($cookie, $target_qq){

        $result = $this->get($cookie['uin'], $cookie['skey'], $target_qq);

        return $result;
    }

    /**
     * @param $uin
     * @param $skey
     * @return mixed
     */
    private function get($uin, $skey, $target_qq){
        $testurl = "http://qlist.qlogo.cn/file_list?src=1&uin={$target_qq}&md5=&start_idx=1&end_idx=500";

        $header = array(
            "User-Agent"=>'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/68.0.3440.106 Safari/537.36;)'
        );
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $testurl);
        //参数为1表示传输数据，为0表示直接输出显示。
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        //参数为0表示不带头文件，为1表示带头文件
        curl_setopt($ch, CURLOPT_HEADER,0);
        curl_setopt($ch,CURLOPT_HTTPHEADER,$header);
        curl_setopt($ch,CURLOPT_COOKIE,"uin={$uin}; skey={$skey}");
        $output = curl_exec($ch);
        curl_close($ch);
        return $output;
    }
}