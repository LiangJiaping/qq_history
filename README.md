# PHP 版获取任意QQ历史头像

#### 项目介绍
利用webdriver打开Chrome浏览器，让其自动登录目标链接，获取Cookies，把cookies写入文件，再利用该Cookies请求头像接口地址，获取历史头像。

#### 插件依赖
服务器：  
[php-webdriver](https://github.com/facebook/php-webdriver)  
[chromedriver](http://chromedriver.chromium.org/)  
[selenium-server](http://selenium-release.storage.googleapis.com/index.html)  
前端：  
[layer](http://layer.layui.com/)  
[Bootstrap](http://www.bootcss.com/)  
[art-template](http://aui.github.io/art-template/zh-cn/)

上面链接请自备梯子访问

#### 环境需求
Windows server  
(要移植到Linux，只需要安装最新的Linux版本的Chrome和ChromeDriver)

#### 安装教程和使用说明

1. 服务器安装最新版的Chrome  
2. 下载与Chrome版本对应的ChromeDriver放到根目录覆盖原来的已内置chromedriver.exe（已内置ChromeDriver 2.41 版本，对应Chrome v66-68，浏览器版本为66到68则无需替换，忽略此步）  
3. 安装Java 8+ （最低版本8，并且已加入系统环境变量）  
4. 在终端切换到项目根目录，运行服务器：java -jar selenium-server-standalone-3.9.1.jar   
5. 修改配置文件：config.php 的$qq和$password变量
6. 部署php网站  
7. 访问index.html

#### 开发者博客
[https://pingxonline.com/](https://pingxonline.com/)