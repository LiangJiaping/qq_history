<?php
/**
 * Created by PhpStorm.
 * User: https://pingxonline.com/
 * Date: 2018-08-12
 * Time: 10:33
 */
use Facebook\WebDriver\Remote\RemoteWebDriver;
use Facebook\WebDriver\Remote\DesiredCapabilities;
use Facebook\WebDriver\WebDriverBy;
use Facebook\WebDriver\WebDriverExpectedCondition;
use Facebook\WebDriver\Chrome\ChromeOptions;


require_once 'vendor/autoload.php';

class QQHelper
{
    private $cookie;

    private $driver;

    public function login($host, $loginUrl, $qq, $password){

        // 设置一些优化Chrome的选项
        $options = new ChromeOptions();

        $options->addArguments(array(
            // 禁止图片加载
            '--disable-images',
            // 禁止插件加载
            "--disable-plugins"
        ));
        $caps = DesiredCapabilities::chrome();
        $caps->setCapability(ChromeOptions::CAPABILITY, $options);

        $this->driver = RemoteWebDriver::create($host, $caps,5000);

        $this->driver->get($loginUrl);

        $this->driver->findElement(WebDriverBy::id('switcher_plogin'))->click();

        $this->driver->findElement(WebDriverBy::id("u"))->sendKeys($qq);//账号

        $this->driver->findElement(WebDriverBy::id("p"))->sendKeys($password);//密码

        $this->driver->findElement(WebDriverBy::id('login_button'))->click();

        // 等待页面加载，直到链接包含https://qzone.qq.com/则认为登录跳转成功
        $this->driver->wait()->until(
            WebDriverExpectedCondition::urlContains("https://qzone.qq.com/")
        );

        // 获取所有的cookies
        $this->cookie = $this->driver->manage()->getCookies();

        // 获取uin
        $arr['uin'] = $this->driver->manage()->getCookieNamed('uin')['value'];
        $arr['uin'] = str_replace('o','', $arr['uin']);
        // 获取skey
        $arr['skey'] = $this->driver->manage()->getCookieNamed('skey')['value'];

        // 关闭浏览器
        $this->driver->close();

        return $arr;

    }
}